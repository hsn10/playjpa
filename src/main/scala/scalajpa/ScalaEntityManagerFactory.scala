/*
    JPA Scala Support for Play Framework 2
    Copyright (C) 2014 Radim Kolar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.filez.play2.scalajpa

import _root_.org.scala_libs.jpa.{ScalaEMFactory}
import javax.persistence.{EntityManager,EntityManagerFactory}

/**
 * Scala enhanced EntityManagerFactory
 */
class ScalaEntityManagerFactory(val name : String, val factory: EntityManagerFactory) extends ScalaEMFactory {

   override
   def openEM() = {
     factory.createEntityManager()
   }

   override
   def closeEM(em: EntityManager) = {
     em.close()
   }

   override
   def getUnitName = name
}