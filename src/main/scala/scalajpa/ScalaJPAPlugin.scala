/*
    JPA Scala Support for Play Framework 2
    Copyright (C) 2014 Radim Kolar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.filez.play2.scalajpa

import com.filez.play2.jpa.{JPAPlugin, EMFactory}
import play.api.{Plugin, Application}
import scala.collection.mutable.HashMap
import javax.persistence._
import org.scala_libs.jpa.ScalaEntityManager

/**
 * Plugin for making Scala enhanced EntityManager.
 * Depends on JPAPlugin for configuration, it has no own configuration.
 */
class ScalaJPAPlugin(app: Application) extends Plugin with EMFactory[ScalaEntityManager] {

   private val scalaFactories: HashMap[String, ScalaEntityManagerFactory] = HashMap()

   /**
    * Route enabled status to JPAPlugin enable status
    */
   override
   def enabled = { 
   app.plugin(classOf[JPAPlugin])
         .getOrElse(throw new RuntimeException("JPA Plugin not active"))
         .enabled
   }

   /**
    * Create instances of ScalaEntityManagerFactory, mirroring factories used
    * in JPAPlugin
    */
   override
   def onStart() = {
      scalaFactories.clear()
      app.plugin(classOf[JPAPlugin])
          .getOrElse(throw new RuntimeException("JPA Plugin not active"))
          .factories.foreach{ case (name: String, emf: EntityManagerFactory ) =>
              scalaFactories(name) = new ScalaEntityManagerFactory(name, emf)
           }
   }

   /**
    * Clear ScalaEntityManagerFactory instance cache.
    */
   override
   def onStop() = scalaFactories.clear()

   override
   def get(name: String): Option[ScalaEntityManager] = {
    val emf = scalaFactories.get(name)
    if (emf.isDefined)
       Some(emf.get.newEM)
    else
       None
   }
}