/*
    JPA Scala Support for Play Framework 2
    Copyright (C) 2014 Radim Kolar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.filez.play2.jpa

import play.api.Application

/**
 * Simple factory trait for providing EntityManager instances as nullable value or
 * as Option 
 */
trait EMFactory[A >: Null] {

  /**
   * Get new instance of entityManager
   * @param name name of entityManagerFactory
   * @return entity manager or null if entityManagerFactory name was invalid
   */
  def em(name: String): A = get(name).getOrElse(null)

  /**
   * Get new instance of entityManager
   * @param name EntityManagerFactory name
   * @return new entityManager instance as Option
   */
  def get(name: String): Option[A]
}

/**
 * Simple factory trait for providing EntityManager instances and
 * throwing Exception on error.
 * Unlike EMFactory this trait has additional implicit parameter Application used
 * for access to JPA Plugin. 
 */
trait EMFactoryObject[+A] {
  /**
   * Get new instance of "default" entityManager
   * @throws RuntimeException if no such manager exists
   */
  def em()(implicit app: Application): A = 
    get("default").getOrElse(throw new RuntimeException("No default EntityManager exists"))

  /**
   * Get new instance of entityManager
   * @param name name of entityManagerFactory
   * @param app Play framework Application used for access to JPAPlugin
   * @return new instance of entity manager
   * @throws RuntimeException on errors during create, such as invalid
   *    factory name.
   */
  def em(name: String)(implicit app: Application): A = 
     get(name)(app).getOrElse(throw new RuntimeException(s"Can not create EntityManager `$name`, name might be invalid or JPA Plugin not active"))

  /**
   * Get new instance of entityManager
   * @param name EntityManagerFactory name
   * @param app Play framework Application used for access to JPAPlugin
   * @return new entityManager instance as Option
   */
  def get(name: String)(implicit app: Application): Option[A]
}