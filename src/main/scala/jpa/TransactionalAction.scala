package com.filez.play2.jpa

import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits._
import scala.concurrent.Future
import javax.persistence._

object Transactional extends ActionBuilder[TransactionalRequest] {

  override
  def invokeBlock[A](request: Request[A], 
                     block: (TransactionalRequest[A]) => Future[Result]): Future[Result] = {
     val em = JPA.em("default")(play.api.Play.current)
     val tx = em.getTransaction()
     tx.begin()
     block(new TransactionalRequest[A](em, request)).transform({ rc =>
       if( tx.getRollbackOnly() )
         tx.rollback()
       else
         tx.commit()
       em.close()
       rc
     }, { rc =>
           tx.rollback()
           em.close()
           rc
          })
  }
}

/**
 * Enhanced Playframework request
 * with EntityManager added as em field.
 */
class TransactionalRequest[A](val em: EntityManager, request: Request[A])
   extends WrappedRequest[A](request)