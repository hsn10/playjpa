/*
    JPA Scala Support for Play Framework 2
    Copyright (C) 2014 Radim Kolar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.filez.play2.jpa

import javax.persistence.EntityManager
import play.api.Application
import play.api.Play

/** Static methods for JPA support */
object JPA extends EMFactoryObject[EntityManager] with Transaction[EntityManager] {

   /**
    * Get newly created EntityManager with given factory
    * @param name factory to be used for creating EntityManager
    * @throws RuntimeException on error
    */
   override
   def get(name: String)(implicit app: Application): Option[EntityManager] = {
      app.plugin(classOf[JPAPlugin])
         .getOrElse(throw new RuntimeException("JPA Plugin not active"))
         .get(name)
   }

   /**
    * Calls block with newly created EntityManager. Transaction is commited at end of block
    * unless its set to RollbackOnly. In that case rollback is executed.
    * @param name name of entity manager factory
    * @param readOnly transaction is read only
    * @param block block to be executed
    */
   override
   def withTransaction[A](name: String, readOnly: Boolean, block: (EntityManager) => A)(implicit app: Application):A = {
      val entityManager = em(name)
      if (readOnly)
      {
          val rc = block(entityManager)
          entityManager.close()
          rc
      }
      else
      {
          val tx = entityManager.getTransaction()
          tx.begin()
          val rc = block(entityManager)
          if (tx.getRollbackOnly())
              tx.rollback()
          else
              tx.commit()
          entityManager.close()
          rc
      }
   }
}
