/*
    JPA Scala Support for Play Framework 2
    Copyright (C) 2014 Radim Kolar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.filez.play2.jpa

import javax.persistence.EntityManager

/**
 * Thread local binding support of EntityManager for Java applications.
 */
@deprecated(message="""Avoid using this programming style in Scala applications as 
  |assigning objects to threads is unstable because thread might change during
  |execution context used for actors and futures""".stripMargin.replaceAll("\n", " "))
object EMLegacy {
   /** thread local storage for entity manager */
   private val entityManager = new ThreadLocal[EntityManager] ();

   /**
    * Binds EntityManager to current thread
    * @param em EntityManager to be bound to current thread or null to remove
    *           binding.
    */
   @deprecated(message="Using this in scala is bad programming practice")
   def bindForCurrentThread(em: EntityManager): Unit = {
      entityManager.set(em)
   }

   /**
    * Unbind EntityManager from current thread
    */
   def unbindFromCurrentThread(): Unit = {
      entityManager.remove()
   }

   @deprecated(message="Using this in scala is bad programming practice")
   /**
    * Get the default EntityManager for this thread.
    * @return entity manager bound to current thread
    * @throws RuntimeException if no EntityManager is bound
    */
   @deprecated(message="Using this in scala is bad programming practice")
   def em(): EntityManager = {
     val em = entityManager.get()
     if (em != null) 
       em
     else
       throw new RuntimeException("No EntityManager bound to this thread")
   }
}