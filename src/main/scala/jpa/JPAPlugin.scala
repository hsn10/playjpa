/*
    JPA Scala Support for Play Framework 2
    Copyright (C) 2014 Radim Kolar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.filez.play2.jpa

import play.api.{Plugin, Application}
import scala.collection.mutable.HashMap
import javax.persistence._
import com.typesafe.config.ConfigValueType

/**
 * Playframework plugin for JPA support in Scala apps
 */
class JPAPlugin(app: Application) extends Plugin with EMFactory[EntityManager] {

  private[play2] val factories: HashMap[String, EntityManagerFactory] = HashMap()

  /**
   * Start plugin, create named instances of entitymanagerfactory
   */
  override
  def onStart() = {
     factories.clear()
     app.configuration.getConfig("jpa").foreach{ conf =>
        for( kv <- conf.entrySet ) {
           if(kv._2.valueType() == ConfigValueType.STRING) {
        	   factories(kv._1) = Persistence.createEntityManagerFactory(
        	      kv._2.unwrapped().asInstanceOf[String]
        	   ) 
           }
        }
     }
  }

  /**
   * Stop plugin, close factories and clean hashmap
   */
  override
  def onStop() = {
     for( em <- factories.values ) {
        em.close()
     }
	 factories.clear()
  }

  /**
   * JPA Plugin is enabled if jpa section in configuration exists or
   * jpaplugin configuration value is not set to disabled.
   */
  override
  def enabled = {
     app.configuration.getString("jpaplugin", Some(Set("enabled", "disabled")))
        .getOrElse("enabled") == "enabled" || 
     app.configuration.getConfig("jpa").isDefined
  }

  /**
   * Get new instance of EntityManager
   * @param name factory used to create EntityManager
   * @return EntityManager or null if no such factory exists
   */
  override
  def em(name: String):EntityManager = {
    super.em(name)
  }

  /**
   * Get new instance of EntityManager
   * @param name factory used to create EntityManager
   * @return Option with newly created EntityManager or None 
   */
  override
  def get(factory: String): Option[EntityManager] = {
    val emf = factories.get(factory)
    if (emf.isDefined)
       Some(emf.get.createEntityManager())
    else
       None
  }
}