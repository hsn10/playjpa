/*
    JPA Scala Support for Play Framework 2
    Copyright (C) 2014 Radim Kolar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.filez.play2.jpa

import play.api.Application

/** Wraps code block with EntityManager transaction */
trait Transaction[+EM] {

   /**
    * Execute block inside read-write transaction created by EntityManagerFactory with
    * "default" name. 
    */
   def withTransaction[A] (block: (EM) => A)(implicit app: Application): A = {
        withTransaction("default", false, block)
   }

  /**
    * Calls block with newly created EntityManager. Transaction is commited at end of block
    * unless its set to RollbackOnly. In that case rollback is executed.
    * @param name name of entity manager factory
    * @param readOnly transaction is read only
    * @param block block to be executed
    */
   def withTransaction[A](name: String, readOnly: Boolean, block: (EM) => A)(implicit app: Application): A
}