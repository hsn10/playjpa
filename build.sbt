organization := "com.filez.play2"

name := "jpa"

version := "0.0.1-SNAPSHOT"


scalaVersion := "2.10.4"

val playVersion = "2.3.7"

libraryDependencies in ThisBuild ++= Seq(
    "com.typesafe.play" %% "play" % playVersion,
    "org.hibernate.javax.persistence" % "hibernate-jpa-2.1-api" % "1.0.0.Final",
    "org.scala-libs" %% "scalajpa" % "1.5"
)

resolvers += Resolver.typesafeRepo("releases")
