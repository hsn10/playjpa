JPA support for Scala Play framework
------------------------------------

This project is support for JPA persistence in Scala applications
written for Play framework because standard Play framework supports JPA only
in Java applications.

### Configuration ###

Plugin needs to be configured in *conf/application.conf* file. Configuration line
should look like **jpa.default=defaultPersistence** where *default* is name of
EntityManagerFactory created for application use and *defaultPersistence* is name
used in &lt;persistence-unit name="..."&gt; xml tag in *conf/META-INF/persistence.xml*
file.
You can use multiple JPA persistence units with different providers.

#### Disable plugin ####

JPA Plugin can be disabled by putting line **jpaplugin=disabled**
in *conf/application.conf*.

### Interesting links ###

Persistence: [JPA](http://www.oracle.com/technetwork/java/javaee/tech/persistence-jsp-140049.html) 2.1

Framework: [Play Framework](http://www.playframework.com/documentation/2.3.x/Home) 2.3

Language: [Scala](http://www.scala-lang.org/) 2.10

License: [AGPL3](http://www.gnu.org/licenses/agpl-3.0.html)

Playframework JPA API: [Java version](https://www.playframework.com/documentation/2.3.x/api/java/index.html?play/db/jpa/JPA.html) for 2.3.x

Playframework DB API: [Scala version](https://www.playframework.com/documentation/2.3.x/api/scala/index.html#play.api.db.DBApi) for 2.3.x

Playframework JPA Source: [Java version](https://github.com/playframework/playframework/tree/master/framework/src/play-java-jpa/src/main/java/play/db/jpa)

Playframework DB Source: [Scala version](https://github.com/playframework/playframework/tree/master/framework/src/play-jdbc/src/main/scala/play/api/db)

